Name:          geolite2
Version:       20181002
Release:       2
Summary:       IP geolocation databases
License:       CC-BY-SA
URL:           https://dev.maxmind.com/geoip/geoip2/geolite2/
Source0:       GeoLite2-City_%{version}.tar.gz
Source1:       GeoLite2-Country_%{version}.tar.gz
BuildArch:     noarch

%description
GeoLite2 databases are free IP geolocation databases. The GeoLite2 Country, City,
and ASN databases are updated weekly, every Tuesday.

%package city
Summary:   The GeoLite2 city database

%description city
The GeoLite2 city database

%package country
Summary:   The GeoLite2 country database

%description country
The GeoLite2 country database

%prep
%setup -q -T -c -a 0 -a 1

%install
for db in GeoLite2-City GeoLite2-Country; do
    install -D -p -m 0644 ${db}_%{version}/$db.mmdb %{buildroot}%{_datadir}/GeoIP/$db.mmdb
done
mv GeoLite2-City_%{version}/COPYRIGHT.txt GeoLite2-City_%{version}/COPYRIGHT
mv GeoLite2-City_%{version}/LICENSE.txt GeoLite2-City_%{version}/LICENSE
mv GeoLite2-Country_%{version}/COPYRIGHT.txt GeoLite2-Country_%{version}/COPYRIGHT
mv GeoLite2-Country_%{version}/LICENSE.txt GeoLite2-Country_%{version}/LICENSE

%files city
%license GeoLite2-City_%{version}/COPYRIGHT GeoLite2-City_%{version}/LICENSE
%verify(not md5 size mtime) %{_datadir}/GeoIP/GeoLite2-City.mmdb

%files country
%license GeoLite2-Country_%{version}/COPYRIGHT GeoLite2-Country_%{version}/LICENSE
%verify(not md5 size mtime) %{_datadir}/GeoIP/GeoLite2-Country.mmdb

%changelog
* Tue Sep 10 2019 Lijin Yang <yanglijin@huawei.com> - 20181002-2
- Package init

